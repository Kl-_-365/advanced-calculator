# итак, приступим к добавлению столь долгожданных и необходимых тригонометрических функций
# в ходе раздумий было принято решение докинуть сверху и логарифмы,
# потому как по трудозатратам это практически одинаково, а впечатление, надеюсь, произведет положительное
# в качестве аргумента тригонометрических функций будем принимать градусы! (не радианы)

# для вычисления будем принимать функции типа Sin(), Cos(), Tg(), Ctg(), Ln(), Lg()
# и заменим для удобства нахождения обозначение этих функций Sin - S, Cos - C, Tg - T, Ctg - G, Ln - N, Lg - L


from calculate import calculation_expression
from abc import ABC, abstractmethod
from decimal import Decimal
from utilits import dict_of_functions


def calculating_functions(expression: str):
    # далее ищем с конца строки наши функции
    for index in range(len(expression) - 1, -1, -1):
        if expression[index] in dict_of_functions.keys():
            # если функция найдена находим её аргумент, это может быть не так просто, как кажется на первый взгляд
            # обнулим счётчик скобочек
            count_breckets = 0
            # установим границы аргумента по-умолчанию
            start = stop = index + 1
            # начинаем считать скобочки
            for symbol in expression[start:]:
                # открывающая - инкремент
                if symbol == '(':
                    count_breckets += 1
                # закрывающая - декремент
                elif symbol == ')':
                    count_breckets -= 1
                # если счётчик скобочек достиг нуля - границы аргумента найдены
                if count_breckets == 0:
                    # вычислим аргумент функции если вдруг там выражение, а не готовое число
                    argument = calculation_expression(expression[start:stop + 1])
                    # возможно в процессе вычисления возникли ошибки
                    if argument.find('EXC') != -1:
                        return argument
                    # если всё в порядке приводим типы
                    argument = float(argument)
                    # теперь вычислим саму функцию (всё самое интересное ниже)
                    name = dict_of_functions[expression[start - 1]]
                    result = Calculator_function.calculate(name, argument)
                    # вставим вычислеммую часть
                    expression = expression[:start - 1] + str(result) + expression[stop + 1:]
                    # могут появиться лишние минусы
                    if start - 2 == 0:
                        if expression[start - 2] == '-':
                            expression = expression[0] + '1*' + expression[1:]
                    # выход
                    break
                # сдвинем границу аргумента на 1 символ
                stop += 1
    return expression


class Function(ABC):
    pi = 3.14159265358979323846

    def __init__(self, name: str, argument: float):
        self.name = name
        self.argument = argument

    @abstractmethod
    def calculate(self):
        pass

    def __str__(self):
        return f'name: {self.name}'


# можно подключить библиотеку math и с лёгкостью всё посчитать, но лёгкие пути не для нас
class Sin(Function):

    def __init__(self, name, argument):
        super().__init__(name, argument)
        # градусы в радианы
        self.argument = (self.argument * Function.pi) / 180

        # учитываем периодичность функции
        while self.argument > 2 * Function.pi:
            self.argument -= 2 * Function.pi

    def calculate(self):

        eps = 0.000000000000001
        result_expression = intermediate_result = self.argument
        n = 1
        # вычисляем синус вручную
        while abs(intermediate_result) > eps:
            intermediate_result = (((-1) ** n) * (self.argument ** (2 * n + 1))) / (self.__factorial(2 * n + 1))
            n += 1
            result_expression += intermediate_result
        return round(result_expression, 14)

    # немножко рекурсии
    def __factorial(self, n):
        if n == 0:
            return 1
        return n * self.__factorial(n - 1)


class Cos(Sin):
    def calculate(self):
        return round((1 - super().calculate() ** 2) ** 0.5, 14)


class Tg(Cos):
    def calculate(self):
        return Sin.calculate(self) / Cos.calculate(self)


class Ctg(Tg):
    def calculate(self):
        return 1 / super().calculate()


class Ln(Function):

    def calculate(self):
        return float(Decimal(self.argument).ln())


class Lg(Ln):

    def calculate(self):
        return float(Decimal(self.argument).log10())


class Calculator_function(object):
    @classmethod
    def calculate(cls, name, argument):
        if name == 'SIN':
            return Sin(name, argument).calculate()
        elif name == 'COS':
            return Cos(name, argument).calculate()
        elif name == 'TG':
            return Tg(name, argument).calculate()
        elif name == 'CTG':
            return Ctg(name, argument).calculate()
        elif name == 'LN':
            return Ln(name, argument).calculate()
        elif name == 'LG':
            return Lg(name, argument).calculate()
