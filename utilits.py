# создадим словарь с вводимыми элементами
# почему в этом файле, а не в файле с функциями?
# потому что иначе возникает ошибка кругового импорта, поэтому как-то так
dict_of_functions = {'S': 'SIN', 'C': 'COS', 'G': 'CTG', 'T': 'TG', 'N': 'LN', 'L': 'LG'}

tuple_of_priority_operators = ('^', '*', '/', '-', '+')


def prioritization():
    # Priority:
    # 1. ()
    # 2. functions
    # 3. ^
    # 4. * or /
    # 5. + or -
    # передадим наш кортеж, в соответствии с порядком приорететов, выполняемых операций
    return tuple_of_priority_operators


def removing_unnecessary_brackets(expression):
    # removing_unnecessary_brackets (удаляем ненужные скобки, появившееся в процессе вычисления)

    # находим скобочку
    step = expression.rfind('(')
    # пока не проверим все скобочки - не сдадимся
    while step != -1:
        # начнём с того, на чём остановились
        start = step
        # ограничимся ближайшей закрывающей скобкой
        end = expression.find(')', start)
        # если внутри осталось одно число - удалим скобочки вокруг него
        if is_float(expression[start + 1:end]):
            expression = expression[:start] + expression[start + 1: end] + expression[end + 1:]
            # не забываем, что при удалении строка смещается
            step -= 2
        # двигаемся дальше
        step -= 1
        # находим новые объекты
        step = expression.rfind('(', 0, step + 1)
    return expression


def is_float(expression: str):
    # если это чисто, то True
    # если это не число, то False
    try:
        float(expression)
    except ValueError:
        return False
    else:
        return True


# больше нигде места не нашлось этому методу, вопрос в следующем, если число стоит возле открывающей скобочки
# без знака, скобочку мы умножаем на число, если минус рядом, то умножаем на -1,
# если две скобочки рядом, то мы должны их перемножить, чтожжжжжж, попробуем реализовать
def adding_multiplication_near_parentheses(expression):
    # ищем нашу скобочку
    step = expression.find('(')
    # всё как всегда, выполняем пока есть непроверенные скобочки
    while step != -1:
        # если скобочка в начале, делать ничего не нужно
        if step == 0:
            pass
        # если перед скобочкой другая скобочка или число, просто добавим знак умножить
        elif expression[step - 1] == ')' or expression[step - 1].isdigit() or expression[step - 1] == '.':
            expression = expression[:step] + '*' + expression[step:]
        # если перед скобочкой знак минус и она находится в самом начале выражения, умножаем её на -1
        elif expression[step - 1] == '-' and step == 1:
            expression = '-1*' + expression[1:]
        # если за минусом сразу же встречаем открывающую скобочку, домнажаем на всё тот же -1
        # оптимизация - скажете вы; ночью нужно спать, а компу всё равно делать нечего - пускай считает - скажу я
        elif expression[step - 1] == '-' and expression[step - 2] == '(':
            expression = expression[:step] + '1*' + expression[step:]
        step += 1
        step = expression.find('(', step)

    # если за скобочкой число и между ними нет оператора - добавим умножение
    # установим шаг на следующем элементе
    if expression.find(')') != -1:
        step = expression.find(')') + 1
    # всё как всегда, выполняем пока есть непроверенные скобочки
    while step != -1:
        # если скобочка в конце - делать ничего не нужно
        if step >= len(expression) - 1:
            pass
        elif expression[step].isdigit() or expression[step] == '.' or expression[step] in dict_of_functions:
            expression = expression[:step] + '*' + expression[step:]
        step = expression.find(')', step + 1)
    return expression
